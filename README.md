
Introduction -

This module prevents the user from login into his account until he enters the code that is sent to his email, otherwise, the user can't log in to his account.

insatallion:

the module require two module to be installed,user module and field_permissions moule

upcoming features:

resend email functionlity
The module is compatible with drupal 8
=======
# Custom 2 Factor Authentication

This module avoids users log in until they enter the code that has been sent
them via email.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cutom_2fa).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cutom_2fa).


## Table of contents

- Requirements
- Installation
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- [Amgad Hassan](https://www.drupal.org/u/amgad-hassan)
