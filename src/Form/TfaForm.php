<?php

namespace Drupal\custom_2fa\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Class TfaForm.
 */
class TfaForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tfa_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state , $uid=NULL) {
    $form['#prefix'] = '<div id="tfa-form">';
    $form['#suffix'] = '</div>';
    $form['#title'] = $this->t("Please enter code received at your email.");
     $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Code'),
      '#title_display' => 'invisible',
      '#description' => $this->t('Enter the code received at your email inbox'),
      '#maxlength' => 6,
      '#size' => 7,
      '#weight' => '0',
    ];
    $form['uid'] = [
      "#type" => "hidden",
      "#value" => base64_decode($uid),
    ];
    $form['actions'] = ["#type" => "actions"];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];
   
    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $uid = $form_state->getValue("uid");
    $code = $form_state->getValue("code");
    $token = \Drupal::entityTypeManager()->getStorage('second_factor')->load($uid);
    if(!$token){
      \Drupal::messenger()->addError($this->t("Invalid Code."));
      $response->addCommand(new ReplaceCommand('#tfa-form', $form));
    }
    elseif ($token->code != md5($code)){
      \Drupal::messenger()->addError($this->t("Invalid Code."));
      $response->addCommand(new ReplaceCommand('#tfa-form', $form));

    }
    elseif((time() - $token->created) >= ($this->config('custom_2fa.settings')->get("code_expiry")*60)) {
      \Drupal::messenger()->addError($this->t("Expired Code."));
      $response->addCommand(new ReplaceCommand('#tfa-form', $form));
    }
    elseif ($token->code == md5($code)){
      $user = \Drupal::service('entity_type.manager')->getStorage('user')->load($uid);
      user_login_finalize($user);
      $token->delete();
      $url = Url::fromRoute('entity.user.canonical', ['user' => $uid]);
      $command = new RedirectCommand($url->toString());
      $response->addCommand($command);
      return $response;
    }
    return $response;
  }

 

  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
