<?php
namespace Drupal\custom_2fa\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Custom 2FA settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_2fa_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['custom_2fa.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = user_role_names();
    unset($options['anonymous']);
    $form["roles_container"] = [
      '#type' => "fieldset",
      "#title" => $this->t("Roles"),
    ];
    $rolesData = $this->config('custom_2fa.settings')->get("roles");
    if ($rolesData == "" || $rolesData == NULL){
      $roles = [];
    }
    else{
      $roles = json_decode($rolesData);
    }
    $defaults = [];
    foreach($roles as $key => $value){
      if ($value != "0"){
        $defaults[] = $key;
      }
    }
    $form["roles_container"]['roles'] =[
      "#type" => "checkboxes",
      "#title" => $this->t("Roles that require 2FA"),
      "#options" => $options,
      "#default_value" => $defaults,
    ];
    $form['email_template'] = ["#type" => "fieldset" , "#title" => $this->t("Email settings")];
    $form['email_template']['subject'] = [
      "#type" => "textfield",
      "#title" => $this->t("OTP Email subject"),
      "#description" => $this->t('You can use {name} , {sitename} tokens for replacement'),
      "#default_value" => $this->config('custom_2fa.settings')->get("subject"),
      "#required" => true,
    ];
    $form['email_template']['body'] = [
      "#type" => "textarea",
      "#title" => $this->t('OTP Email body'),
      "#description" => $this->t('You can use {name} , {sitename} , {code} tokens for replacement'),
      "#default_value" => $this->config('custom_2fa.settings')->get("body"),
      "#required" => true,
    ];

    $form['code_length_container'] = ['#type' => "fieldset","#title" => $this->t("Code Length"),];
    $form['code_length_container']['code_length'] = [
      "#type" => "number",
      "#title" => $this->t("Code Length"),
      "#step" => 1,
      "#min" => 1,
      "#max" => 10,
      "#default_value" => $this->config('custom_2fa.settings')->get("code_length"),
      "#size" =>11,
      "#required" => true,
    ];

    $form['code_expiry_container'] = ['#type' => "fieldset","#title" => $this->t("Code Expiry"),];
    $form['code_expiry_container']['code_expiry'] = [
      "#type" => "number",
      "#title" => $this->t("Code Expiry"),
      "#description" => $this->t("in minutes"),
      "#required" => true,
      "#step" => 1,
      "#min" => 1,
      "#max" => 60,
      "#default_value" => $this->config('custom_2fa.settings')->get("code_expiry"),
      "#size" =>11
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('custom_2fa.settings')
      ->set("roles", json_encode($form_state->getValue("roles")))
      ->set("subject", $form_state->getValue("subject"))
      ->set("body", $form_state->getValue("body"))
      ->set("code_length", $form_state->getValue("code_length"))
      ->set("code_expiry", $form_state->getValue("code_expiry"))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
