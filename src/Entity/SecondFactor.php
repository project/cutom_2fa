<?php

namespace Drupal\custom_2fa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Second factor entity.
 *
 * @ConfigEntityType(
 *   id = "second_factor",
 *   label = @Translation("Second factor"),
 *   config_prefix = "second_factor",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "code" = "code"
 *   }
 * )
 */
class SecondFactor extends ConfigEntityBase implements SecondFactorInterface {

  /**
   * The Second factor ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Second factor code.
   *
   * @var string
   */
  public $code;
  
  /**
   * The Second factor status.
   *
   * @var string
   */
  public $expired;

  /**
   * The Second factor created.
   *
   * @var string
   */
  public $created;

  



  public function preSave(EntityStorageInterface $storage){
      parent::preSave($storage);
      while (TRUE) {
        $code = $this->generateCode();
        $codes = \Drupal::entityTypeManager()->getStorage('second_factor')->getQuery()
          ->condition("code" , $code)
          ->execute();
        if(count($codes) == 0){
          break;
        }
      }
      $this->code = md5($code);
      $this->created = time();
      $this->sendOTP($code);
  }

  public function gen(){
    while (TRUE) {
        $code = $this->generateCode();
        $codes = \Drupal::entityTypeManager()->getStorage('second_factor')->getQuery()
          ->condition("code" , $code)
          ->execute();
        if(count($codes) == 0){
          break;
        }
      }
      $this->code = md5($code);
      $this->created = time();
      return $code;

  }

  public function generateCode($mode = "Numeric"){
    if ($mode == 'Numeric'){
      $characters = '0123456789';
    }
    else{
      $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < \Drupal::config('custom_2fa.settings')->get("code_length"); $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public function sendOTP($code){
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($this->id());
    $email = $this->composeEmail($user->name->value , $code);
    //\Drupal::logger('custom_2fa')->notice($email['subject'] . "//" .$email['body']);
    $mailManager = \Drupal::service('plugin.manager.mail');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $mailManager->mail("custom_2fa", "OTP", $user->getEmail(), $langcode , $email, \Drupal::config('system.site')->get('mail'), $send = TRUE);
  }

  public function composeEmail($name , $code){
    $siteName = t(\Drupal::config('system.site')->get('name'));
    $subject = \Drupal::config('custom_2fa.settings')->get("subject");
    $subject = str_replace('{sitename}', $siteName, $subject);
    $subject = str_replace('{name}', $name, $subject);
    $body =  \Drupal::config('custom_2fa.settings')->get("body");
    $body = str_replace('{name}', $name, $body);
    $body = str_replace('{sitename}', $siteName, $body);
    $body = str_replace('{code}', $code, $body);
    return ["subject"=>$subject , "body"=>$body];
  }



  public function checkExpiry(){
    if (time() - $this->created >= 1800){
      return TRUE;
    }
    else{
      return FALSE;
    }
  }
}
