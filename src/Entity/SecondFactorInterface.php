<?php

namespace Drupal\custom_2fa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Second factor entities.
 */
interface SecondFactorInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
	
}
