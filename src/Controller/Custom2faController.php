<?php

namespace Drupal\custom_2fa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Url;

/**
 * Returns responses for Custom 2FA routes.
 */
class Custom2faController extends ControllerBase {
  
  public function __construct() {
    $this->userStorage = \Drupal::service('entity_type.manager')->getStorage('user');
    $this->userAuth = \Drupal::service('user.auth');
    $this->flood = \Drupal::service('flood');
    $this->routeProvider = \Drupal::service('router.route_provider');
    $this->logger = \Drupal::service('logger.factory')->get('user');
  }

  /**
   * Callback for opening the modal form.
   */
  public function openModalForm($uid) {
    $response = new AjaxResponse();
    $modal_form = \Drupal::formBuilder()->getForm('Drupal\custom_2fa\Form\TfaForm',$uid);
    $response->addCommand(new OpenModalDialogCommand($this->t('Please verify your identity'), $modal_form, ['width'=>'50%','closeOnEscape'=>'false']));
    return $response;
  }

  /**
   * Builds the response.
   */
  public function build($uid) {
    $form['#title'] = "";
    $form['open_modal'] = [
      '#type' => 'link',
      '#title' => $this->t('Open Modal'),
      '#url' => Url::fromRoute('custom_2fa.open_modal_form',['uid'=>$uid]),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
        "style" => ["display:none;"],
        "id" => ["tfa-modal"]
      ],
    ];
    // Attach the library for pop-up dialogs/modals.
    $form['#attached']['library'][] = 'custom_2fa/custom_2fa';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $form;
  }

  public function login($username , $password){
    $credentials = ["name" => $username , "pass" => $password];
    $this->floodControl( $credentials['name']);
    if (user_is_blocked($credentials['name'])) {
      \Drupal::messenger()->addError(t('The user has not been activated or is blocked.'));
      $response = new RedirectResponse("/user/login");
      $response->send();
    }
    if ($uid = $this->userAuth->authenticate($credentials['name'], $credentials['pass'])) {
      $this->flood->clear('user.http_login', $this->getLoginFloodIdentifier($credentials['name']));
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->userStorage->load($uid);
      //check roles
      $verifiableRole = FALSE;
      $rolesData = $this->config('custom_2fa.settings')->get("roles");
      if ($rolesData == "" || $rolesData == NULL){
        $roles = [];
      }
      else{
        $roles = json_decode($rolesData);
      }
      foreach($roles as $key => $value){
        if ($value != "0" && $user->hasRole($key)){
          $verifiableRole = TRUE;
        }
      }
      //check user
      $verifiableUser = FALSE;
      if($user->field_force_2fa_authentication->value == 1){
         $verifiableUser = TRUE;
      }
      if (isset( $_GET['destination'] )){
        $destination = $_GET['destination'];
      }
      else{
        $destination = "/user/$uid";
      }
      if (($verifiableUser || $verifiableRole)&& $uid != 1){ 
        $token = \Drupal::entityTypeManager()->getStorage('second_factor')->load($uid);
        if (!$token){
          $token = \Drupal::entityTypeManager()->getStorage('second_factor')->create([
            "id" => $uid,
          ]);
        }
        $token->save();
        $response = new RedirectResponse("/tfa-checkpoint/" . urlencode(base64_encode($uid)));
        $response->send();
        //return;
      }
      else{
        user_login_finalize($user);
        $response = new RedirectResponse($destination);
        $response->send();
      }
    }
    else{
      $flood_config = \Drupal::config('user.flood');
      if ($identifier = $this->getLoginFloodIdentifier($credentials['name'])) {
        $this->flood->register('user.http_login', $flood_config->get('user_window'), $identifier);
      }
      // Always register an IP-based failed login event.
      $this->flood->register('user.failed_login_ip', $flood_config->get('ip_window'));
      \Drupal::messenger()->addError(t('Sorry, unrecognized username or password.'));
      $response = new RedirectResponse("/user/login");
      $response->send();
    }
  }
/**
   * Enforces flood control for the current login request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param string $username
   *   The user name sent for login credentials.
   */
  protected function floodControl( $username) {
    $flood_config = \Drupal::config('user.flood');
    if (!$this->flood->isAllowed('user.failed_login_ip', $flood_config->get('ip_limit'), $flood_config->get('ip_window'))) {
      throw new AccessDeniedHttpException('Access is blocked because of IP based flood prevention.', NULL, Response::HTTP_TOO_MANY_REQUESTS);
    }
    if ($identifier = $this->getLoginFloodIdentifier( $username)) {
      // Don't allow login if the limit for this user has been reached.
      // Default is to allow 5 failed attempts every 6 hours.
      if (!$this->flood->isAllowed('user.http_login', $flood_config->get('user_limit'), $flood_config->get('user_window'), $identifier)) {
        if ($flood_config->get('uid_only')) {
          $error_message = sprintf('There have been more than %s failed login attempts for this account. It is temporarily blocked. Try again later or request a new password.', $flood_config->get('user_limit'));
        }
        else {
          $error_message = 'Too many failed login attempts from your IP address. This IP address is temporarily blocked.';
        }
        throw new AccessDeniedHttpException($error_message, NULL, Response::HTTP_TOO_MANY_REQUESTS);
      }
    }
  }

  /**
   * Gets the login identifier for user login flood control.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param string $username
   *   The username supplied in login credentials.
   *
   * @return string
   *   The login identifier or if the user does not exist an empty string.
   */
  protected function getLoginFloodIdentifier( $username) {
    $flood_config = \Drupal::config('user.flood');
    $accounts = $this->userStorage->loadByProperties(['name' => $username, 'status' => 1]);
    if ($account = reset($accounts)) {
      if ($flood_config->get('uid_only')) {
        // Register flood events based on the uid only, so they apply for any
        // IP address. This is the most secure option.
        $identifier = $account->id();
      }
      else {
        // The default identifier is a combination of uid and IP address. This
        // is less secure but more resistant to denial-of-service attacks that
        // could lock out all users with public user names.
        $identifier = $account->id() . '-' . $_SERVER['REMOTE_ADDR'];
      }
      return $identifier;
    }
    return '';
  }

}
